package com.example.flow

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val countDownFlow = flow<Int> {
        val startingValue = 10
        var currentValue = startingValue
        emit(startingValue)
        while (currentValue > 0) {
            delay(1000L)
            currentValue--
            emit(currentValue)
        }
    }

    fun collectFlow() {
        countDownFlow.onEach {
            Log.d(TAG, "onEach1: $it")
        }
            .launchIn(viewModelScope)


        viewModelScope.launch {
            countDownFlow
                .filter { it % 2 == 0 }
                .map { it * it }
                .onEach {
                    Log.d(TAG, "onEach2: value=$it")
                }
                .collect {
                    Log.d(TAG, "collectFlow: value=$it")
                }
        }

        viewModelScope.launch {
            val count = countDownFlow.filter { it % 2 == 0 }
                .count {
                    it >= 2
                }
            Log.d(TAG, "the count is $count")
        }

        viewModelScope.launch {
            val reduce = countDownFlow.filter { it % 2 == 0 }
                .reduce { accumulator, value ->
                    accumulator + value
                }
            Log.d(TAG, "the reduce is $reduce")
        }

        viewModelScope.launch {
            val fold = countDownFlow.filter { it % 2 == 0 }
                .fold(100) { accumulator, value ->
                    accumulator + value
                }
            Log.d(TAG, "the fold is $fold")
        }
    }

    fun flattenFlow() {
        val flow1 = flow {
            emit(1)
            delay(500L)
            emit(2)
        }

        val flow2 = flow {
            emit(10)

        }

        viewModelScope.launch {
            flow1.flatMapConcat { value ->
                flow {
                    emit(value + 1)
                    delay(500L)
                    emit(value + 2)
                }
            }.collect { value ->
                Log.d(TAG, "flattenFlow: The value is $value")
            }
        }
    }

    fun buffer() {
        val flow = flow {
            delay(250L)
            emit("Appetizer")
            delay(1000L)
            emit("Main Dish")
            delay(100L)
            emit("Dessert")
        }

        viewModelScope.launch {
            flow.onEach {
                Log.d(TAG, "buffer: FLOW $it delivered")
            }
                .buffer()
                .collect {
                    Log.d(TAG, "buffer: FLOW $it eating")
                    delay(5000L)
                    Log.d(TAG, "buffer: FLOW $it finished eating")
                }
        }
    }

    fun conflate() {
        val flow = flow {
            delay(250L)
            emit("Appetizer")
            delay(1000L)
            emit("Main Dish")
            delay(100L)
            emit("Dessert")
        }

        viewModelScope.launch {
            flow.onEach {
                Log.d(TAG, "conflate: FLOW $it delivered")
            }
                .conflate()
                .collect {
                    Log.d(TAG, "conflate: FLOW $it eating")
                    delay(5000L)
                    Log.d(TAG, "conflate: FLOW $it finished eating")
                }
        }
    }

    fun collectLatest() {
        val flow = flow {
            delay(250L)
            emit("Appetizer")
            delay(1000L)
            emit("Main Dish")
            delay(100L)
            emit("Dessert")
        }

        viewModelScope.launch {
            flow.onEach {
                Log.d(TAG, "collectLatest: FLOW $it delivered")
            }
                .collectLatest {
                    Log.d(TAG, "collectLatest: FLOW $it eating")
                    delay(5000L)
                    Log.d(TAG, "collectLatest" +
                            ": FLOW $it finished eating")
                }
        }
    }

    companion object {
        val TAG = MainViewModel::class.java.toString()
    }
}