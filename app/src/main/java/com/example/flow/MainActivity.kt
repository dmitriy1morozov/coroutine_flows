package com.example.flow

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    val viewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.btnStart).setOnClickListener {
            viewModel.collectFlow()
            startTimer()
        }

        findViewById<Button>(R.id.btnFlatten).setOnClickListener {
            viewModel.flattenFlow()
        }

        findViewById<Button>(R.id.btnBuffer).setOnClickListener {
            viewModel.buffer()
        }

        findViewById<Button>(R.id.btnConflate).setOnClickListener {
            viewModel.conflate()
        }

        findViewById<Button>(R.id.btnCollectLatest).setOnClickListener {
            viewModel.collectLatest()
        }
    }

    private fun startTimer() {
        val tvTimer = findViewById<TextView>(R.id.tvTimer)

        lifecycleScope.launch {
            viewModel.countDownFlow.collect {
                tvTimer.text = it.toString()
            }
        }
    }
}